package repositories;

import models.Car;

import java.util.List;

public interface CarRepository {
    List<Car> findAll();

    List<Car> findByColorOrMileage(String color, Integer mileage);

    List<Car> numberOfUniqueModels(Integer from, Integer to);

    void colorWithMinPrice();

    void averagePrice();
}
