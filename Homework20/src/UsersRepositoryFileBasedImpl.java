import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class UsersRepositoryFileBasedImpl {

    private String fileName;

    public UsersRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    // возвращает список всех пользователей из файла
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileName));
            String text = reader.readLine();
            while (text != null) {
                String[] user = text.replaceAll(" ", "").split("\\|");
                users.add(new User(user[0], user[1]));
                text = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return users;
    }
        // поиск по имени
        public Optional<User> findByFirstName (String firstName) {
            BufferedReader reader = null;

            try {
                reader = new BufferedReader(new FileReader(fileName));
                String text = reader.readLine();
                while (text != null) {
                    String[] user = text.replaceAll(" ", "").split("\\|");
                    User users = new User(user[0], user[1]);
                    if (firstName.equals(user[0])) {
                        return Optional.of(users);
                    }
                    text = reader.readLine();
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException ignore) {
                    }
                }
            }
            return Optional.empty();
        }

        public void save (User user) {
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(fileName, true));
                writer.write(user.getFirstName() + " | " + user.getLastName());
                writer.newLine();
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            } finally {
                //выполняется всегда, вне зависимости от ошибки
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException ignore) {
                }
            }
        }
    }
}
