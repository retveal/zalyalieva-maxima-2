import java.util.*;

public class Bank {

    private Map<User, List<Transaction>> transactions;

    public Bank() {
        this.transactions = new HashMap<>();
    }
    // метод для перевода денег
    public void sendMoney(User from , User to, int sum) {
        // создаем сам перевод
        Transaction transaction = new Transaction(from, to, sum);
        // проверить, если ли уже у нас история переводов этого пользователя или нет
        // если еще переводов не было
        if (!transactions.containsKey(from)) {
            // кладу пользователю пустой список
            transactions.put(from, new ArrayList<>());
        }
        // get - получает список всех транзакций пользователя from и добавляет транзакцию в этот список
        transactions.get(from).add(transaction);
    }

    // вернуть сумму всех транзакций по конкретному пользователю
    public int getTransactionsSum(User user) {
        int transactionsAmount = 0;

        List<Transaction> userTransactions = transactions.get(user);

        // проверим, делал ли пользователь переводы денежных средств
        // если он не совершал транзакций
        if (!transactions.containsKey(user)) {
            // возвращаем 0
            return 0;
        } else {
            // в противном случае пробегаемся по всем транзакциям данного пользователя
            for (Transaction transaction : userTransactions) {
                // находим их сумму
                transactionsAmount += transaction.getTransactionsSum();
            }
            // возвращаем результат
            return transactionsAmount;
        }
    }
    // метод для получения кол-ва транзакций конкретного пользователя
    public int getTransactions(User user) {
        // проверим, делал ли пользователь переводы денежных средств
        // если он не совершал транзакций
        if (!transactions.containsKey(user)) {
            // возвращаем 0
            return 0;
        } else {
            // в противном случае пробежимся по всем транзакциям данного пользователя
            List<Transaction> userTransactions = transactions.get(user);
            // выведем кол-во этих транзакций
            return userTransactions.size();
        }
    }

    // вывести все имена пользователей и количество их транзакций
    public void userAndTransactions() {
        Set<User> users = getUsers();
        // пробегаемся по всем пользователям
        for (User user : users) {
            // выводим имена пользователей и их транзакции
            System.out.println("Пользователь " + user + " совершил " + getTransactions(user) + " транзакции на общую сумму " + getTransactionsSum(user) + " рублей.");
        }
        System.out.println("Остальные пользователи еще не совершали транзакций.");
    }

    private Set<User> getUsers() {
        return transactions.keySet();
    }
}
