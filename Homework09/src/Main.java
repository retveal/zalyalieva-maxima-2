public class Main {

    // У водителя сделать метод `drive`, который заставит "поехать" автобус.
    //Когда автобус едет, все его пассажиры называют свои имена.
    //Когда автобус едет, нельзя менять водителя, нельзя, чтобы пассажиры покидали свои места,
    //нельзя, чтобы автобус принимал пассажиров.

    public static void main(String[] args) {
        Driverr driver1 = new Driverr("Семен", 10);
        Driverr driver2 = new Driverr("Антон", 5);
        Driverr driver3 = new Driverr("Евгений", 6);
        Driverr driver4 = new Driverr("Аркадий", 1);
        Bus bus = new Bus(99, "Нефаз", 5);
        Passenger passenger1 = new Passenger("Марсель");
        Passenger passenger2 = new Passenger("Виктор");
        Passenger passenger3 = new Passenger("Айрат");
        Passenger passenger4 = new Passenger("Сергей");
        Passenger passenger5 = new Passenger("Кирилл");
        Passenger passenger6 = new Passenger("Иван");

        passenger1.goToBus(bus);
        passenger2.goToBus(bus);
        passenger3.goToBus(bus);
        passenger4.goToBus(bus);

        bus.setDriver(driver1);

        driver1.drive(bus);

        bus.setDriver(driver4);

        driver1.leftBus(bus);

        driver4.change(bus);

        passenger1.pronouncedTheName(bus);
        passenger2.pronouncedTheName(bus);
        passenger3.pronouncedTheName(bus);
        passenger4.pronouncedTheName(bus);

        passenger1.leaveBus(bus);

    }
}
