import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class TextConverter {

    public static void toLowerCaseAll(String sourceFileName, String targetFileName) throws Exception {

        FileInputStream inputStream;

        try {
            inputStream = new FileInputStream(sourceFileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        // используя массив символов вернем кол-во байтов
        char[] characters;
        try {
            characters = new char[inputStream.available()];
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        int position = 0;

        try {
            // считываем текущий байт
            int currentByte = inputStream.read();

            while (currentByte != -1) {
                // преобразуем байт в символ
                char character = (char) currentByte;
                // если символ - буква или пробел
                if (Character.isLetter(character) | character == 32) {
                    // кидаем его в массив
                    characters[position] = character;
                    position++;
                }
                // считываем следующий байт
                currentByte = inputStream.read();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        // создаем строку на основе массива
        String text = new String(characters);

        FileOutputStream outputStream = new FileOutputStream(targetFileName);

        // заменим все символы на строчные
        byte[] bytes = text.toLowerCase().getBytes();

        outputStream.write(bytes);
    }
}
