public class MapArray<S, V> implements Map<S, V> {

    private static final int MAX_MAP_SIZE = 10;
    private int count;

    private Entry<S, V>[] entries;

    public MapArray() {
        this.entries = new Entry[MAX_MAP_SIZE];
    }

    public void put(S string, V value) {
        int hash = Math.abs(string.hashCode() % MAX_MAP_SIZE);
        entries[hash] = new Entry<> (string, value);
    }

    private static class Entry<S, V> {
        S string;
        V value;

        Entry(S string, V value) {
            this.string = string;
            this.value = value;
        }
    }
}
