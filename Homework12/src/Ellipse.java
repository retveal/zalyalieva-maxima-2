public class Ellipse extends Figure {

    // два радиуса
    private double r1;
    private double r2;

    public Ellipse(Center center, double r1, double r2) {
        super(center);
        this.r1 = r1;
        this.r2 = r2;
    }
    @Override
    public double getPerimeter() {
        return 2 * Math.PI * Math.sqrt((r1 * r1 + r2 * r2) / 2);
    }

    public void printPerimeter() {
        System.out.println("Периметр эллипса равен " + getPerimeter());
        getPerimeter();
    }
}
