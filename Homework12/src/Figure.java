public class Figure {

    private final Center center;

    public Figure(Center center) {
        this.center = center;
    }

    // координаты центра фигуры
    protected static class Center{
        int x;
        int y;

        Center(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    public double getPerimeter() {
        return -1;
    }

    public void printPerimeter() {
        return;
    }

    public void move(int newX, int newY) {
        int l1 = newX - center.x;
        int l2 = newY - center.y;
        center.x += l1;
        center.y += l2;
    }
}
