// Реализовать классы Circle и Ellipse (очевидно, что Circle - потомок Ellipse)
// Необходимо, чтобы в свою очередь Ellipse и Rectangle были потомками Figure
// У Figure необходимо описать два поля - x и y (координаты центра фигуры)
// Реализовать метод move, который переносит фигуры в новые координаты
// В каждом классе необходимо переопределить метод getPerimeter.

// В main создать экземпляр каждого класса и показать, что они работают (вызвать периметр у каждой фигуры).
// Каждую фигуру перенести в точку (66, 77)

public class Main {

    public static void main(String[] args) {
        Figure figure = new Figure(new Figure.Center(0,0));
        Figure ellipse = new Ellipse(new Figure.Center(1,1), 13, 7);
        Figure circle = new Circle(new Figure.Center(2,2), 4);
        Figure rectangle = new Rectangle(new Figure.Center(3,3), 6,9);
        Figure square = new Square(new Figure.Center(-1,-1),6);

        ellipse.printPerimeter();
        circle.printPerimeter();
        rectangle.printPerimeter();
        square.printPerimeter();

        figure.move(66,77);
        ellipse.move(66,77);
        circle.move(66,77);
        rectangle.move(66,77);
        square.move(66,77);
    }
}
