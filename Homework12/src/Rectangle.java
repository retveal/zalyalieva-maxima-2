public class Rectangle extends Figure {
    // две стороны, потому что другие две - им равны
    private double a;
    private double b;

    public Rectangle(Center center, double a, double b) {
        super(center);
        this.a = a;
        this.b = b;
    }

    @Override
    public double getPerimeter() {
        return (a + b) * 2;
    }

    public void printPerimeter() {
        System.out.println("Периметр прямоугольника равен " + getPerimeter());
        getPerimeter();
    }
}
