package repositories;

import models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductsRepository {

    void save (Product product);

    List<Product> findAllByPrice(double price);

    Optional<Product> findById(Long id);

    Optional<Product> findOneByName (String product_name);

    void update(Product product);

    void delete(Product product);
}
