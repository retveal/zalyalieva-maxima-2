package repositories;

import models.Product;
import util.jdbc.RowMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductsRepositoryJdbcImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_INSERT_INTO_FOOD_STUFFS = "insert into food_stuffs(product_name, price, supplier) " +
            "values (?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_BY_PRODUCT_NAME = "select * from food_stuffs" +
            " where product_name = ? limit 1";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_PRICE = "select * from food_stuffs" +
            " where price = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from food_stuffs " +
            "where id = ?";

    //language=SQL
    private static final String SQL_UPDATE_PRODUCT_BY_ID = "update food_stuffs set product_name = ?, price = ?, expiration_date = ?, supplier = ? " +
            "where id = ?";

    //language=SQL
    private static final String SQL_DELETE_PRODUCT_BY_ID = "delete from food_stuffs where id = ?";

    private final DataSource dataSource;

    public ProductsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final RowMapper<Product> productsRowMapper = row -> new Product(
            row.getString("product_name"),
            row.getDouble("price"),
            row.getString("supplier")
    );

    @Override
    public void save(Product product) {
        try (Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_INTO_FOOD_STUFFS, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, product.getProduct_name());
            statement.setDouble(2, product.getPrice());
            statement.setString(3, product.getSupplier());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert product");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            // если сгенерированных ключей нет
            if (!generatedKeys.next()) {
                throw new SQLException("Can't retrieve generated id");
            }

            Long generatedId = generatedKeys.getLong("id");

            product.setId(generatedId);

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        List<Product> products = new ArrayList<Product>();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL_BY_PRICE)) {
            statement.setDouble(1,price);

            try (ResultSet resultSet = statement.executeQuery()) {
                // проверяем, есть ли там данные
                while (resultSet.next()) {
                    // если они есть, создаем товар на основе этих данных
                    Product product = productsRowMapper.mapRow(resultSet);
                    products.add(product);
                }
                return products;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<Product> findById(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {

            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                // проверяем, есть ли там данные
                if (resultSet.next()) {
                    // если они есть - создаем пользователя на основе этих данных
                    Product product = productsRowMapper.mapRow(resultSet);

                    if (resultSet.next()) {
                        throw new SQLException("More than one rows");
                    }

                    return Optional.of(product);
                }
                // если данных не нашли
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


    @Override
    public Optional<Product> findOneByName(String product_name) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_PRODUCT_NAME)) {
            statement.setString(1, product_name);

            try (ResultSet resultSet = statement.executeQuery()) {
                // проверяем, есть ли там данные
                if (resultSet.next()) {
                    // если они есть, создаем товар на основе этих данных
                    Product product = productsRowMapper.mapRow(resultSet);
                            if (resultSet.next()) {
                                throw new IllegalArgumentException("More than one rows");
                            }
                    return Optional.of(product);
                }
                // если данные не нашли
                return Optional.empty();
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PRODUCT_BY_ID)) {

            statement.setString(1, product.getProduct_name());
            statement.setDouble(2, product.getPrice());
            statement.setString(3, product.getExpiration_date());
            statement.setString(4, product.getSupplier());
            statement.setLong(5, product.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update product");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(Product product) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_DELETE_PRODUCT_BY_ID)) {

            statement.setLong(1, product.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't delete user");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }
}
