package repositories;

import models.Product;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_INSERT_INTO_FOOD_STUFFS = "insert into food_stuffs(product_name, price, supplier) " +
            "values (?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_BY_PRODUCT_NAME = "select * from food_stuffs" +
            " where product_name = ? limit 1";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_PRICE = "select * from food_stuffs" +
            " where price = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from food_stuffs " +
            "where id = ?";

    //language=SQL
    private static final String SQL_UPDATE_PRODUCT_BY_ID = "update food_stuffs set product_name = ?, price = ?, expiration_date = ?, supplier = ? " +
            "where id = ?";

    //language=SQL
    private static final String SQL_DELETE_PRODUCT_BY_ID = "delete from food_stuffs " +
            "where id = ?";


    private final JdbcTemplate jdbcTemplate;

    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productsRowMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .product_name(row.getString("product_name"))
            .price(row.getDouble("price"))
            .expiration_date(row.getString("expiration_date"))
            .supplier(row.getString("supplier"))
            .build();

    @Override
    public void save(Product product) {
        // объект, который будет сохранять сгенерированные ключи для данного запроса
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement statement = connection.prepareStatement(SQL_INSERT_INTO_FOOD_STUFFS, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, product.getProduct_name());
                statement.setDouble(2, product.getPrice());
                statement.setString(3, product.getSupplier());

                return statement;
            }
        }, keyHolder);

        Long generatedId = ((Integer) keyHolder.getKeys().get("id")).longValue();
        product.setId(generatedId);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE, productsRowMapper, price);
    }

    @Override
    public Optional <Product> findById(Long id) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, productsRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Product> findOneByName(String product_name) {
        try {
            return Optional.of(jdbcTemplate.queryForObject(SQL_SELECT_BY_PRODUCT_NAME, productsRowMapper, product_name));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void update(Product product) {
        jdbcTemplate.update(SQL_UPDATE_PRODUCT_BY_ID,
                product.getProduct_name(),
                product.getPrice(),
                product.getExpiration_date(),
                product.getSupplier(),
                product.getId());
    }

    @Override
    public void delete(Product product) {
        jdbcTemplate.update(SQL_DELETE_PRODUCT_BY_ID,
                product.getProduct_name(),
                product.getPrice(),
                product.getExpiration_date(),
                product.getSupplier(),
                product.getId());
    }
}
