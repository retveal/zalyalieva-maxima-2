public class Main {

    //Написать рекурсивную функцию int sumOfDigits(int number) - возвращает сумму цифр числа.

    public static int sumOfDigits(int number) {

        //126 = 126 / 10 = 6 126 / 10 = 12.6 0 + 6
        // 126 / 10 6 + 12 18

        // на входе
        System.out.println("->> f(" + number + ")");

        if (number == 0) {
            System.out.println("<<-- f() = " + 0);
            return 0;
        }
        int result = number % 10 + sumOfDigits(number / 10);
        System.out.println("<<-- f() = " + result);
        return result;
    }

    public static void main(String[] args) {
        System.out.println(sumOfDigits(2187));
    }
}
