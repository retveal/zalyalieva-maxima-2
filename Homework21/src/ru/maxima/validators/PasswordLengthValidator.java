package ru.maxima.validators;

import ru.maxima.validators.exceptions.PasswordValidationException;

/**
 * 01.02.2022
 * 26. Console Application
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PasswordLengthValidator implements PasswordValidator {
    @Override
    public void validate(String password) throws PasswordValidationException {
        if (password.length() < 5) {
            throw new PasswordValidationException();
        }
    }
}
