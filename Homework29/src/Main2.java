import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import services.ProductsService;
import validators.ProductNameValidator;

public class Main2 {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");

        ProductsService productsService = context.getBean(ProductsService.class);
        productsService.signUp("Tomato juice", 150.0, "Rich");
    }
}
