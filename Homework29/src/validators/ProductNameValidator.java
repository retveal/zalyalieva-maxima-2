package validators;

import validators.exceptions.ProductNameValidatorException;

public interface ProductNameValidator {
    void validate(String product_name) throws ProductNameValidatorException;
}
